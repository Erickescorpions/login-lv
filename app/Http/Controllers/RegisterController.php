<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    public function show() {
        if(auth()->check()) return redirect()->route('home.index');
        return view('auth.register');
    }

    public function register(RegisterRequest $request) {
        $data = $request->validated();
        $user = User::create($data);

        auth()->login($user);
        return redirect()->route('home.index'); 
    }
}
