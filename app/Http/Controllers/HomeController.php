<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        if(!auth()->check()) return redirect('/');

        $users = User::all()->except(auth()->id());

        return view('home', compact('users'));
    }
}
