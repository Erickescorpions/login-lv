<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title')</title>
  @Vite('resources/css/app.css')
</head>
<body>
  {{-- header --}}
  <header>
    {{-- nav --}}
    <h1>
      <a href="{{ auth()->check() ? route('home.index') : route('welcome') }}">Login App</a>
    </h1>
    <nav>
      <ul>
        @guest
          <li><a href="{{ route('login.show') }}">Login</a></li>
          <li><a href="{{ route('register.show') }}">Register</a></li>
        @endguest

        @auth
          <li><a href="{{ route('users.show', auth()->user()->id) }}">Account</a></li>
          <li><a href="{{ route('logout.logout') }}">Logout</a></li>
        @endauth
      </ul>
    </nav>
  </header>

  <main id="main-container">
    @yield('content')
  </main>

</body>
</html>