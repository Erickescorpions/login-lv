@extends('layouts.page')
@Vite('resources/css/form.css')

@section('title', 'Login')

@section('content')
  <div class="container">
    <form action="{{ route('login.login') }}" method="POST">
      @csrf
      <label for="username">Username / Email: </label>
      <input type="text" name="username" id="username">
      <label for="password">Password: </label>
      <input type="password" name="password" id="password">
      <input type="submit" value="Login">
    </form>
  </div>
@endsection
