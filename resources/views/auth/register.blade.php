@extends('layouts.page')
@Vite('resources/css/form.css')

@section('title', 'Register')

@section('content')
  <div class="container">
    <form action="{{ route('register.register') }}" method="POST">
      @csrf
      <label for="username">Username: </label>
      <input type="text" name="username" id="username">
      <label for="email">Email: </label>
      <input type="email" name="email" name="email" id="email">
      <label for="password">Password: </label>
      <input type="password" name="password" id="password">
      <label for="password_confirmation">Password Confirmation: </label>
      <input type="password" name="password_confirmation" id="password_confirmation">
      <input type="submit" value="Register">
    </form>
  </div>
@endsection
