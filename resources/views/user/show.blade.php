@extends('layouts.page')
@Vite('resources/css/show.css')

@section('title', 'Account')

@section('content')
  <div class="container">
    <img class="user-img" src="{{ Storage::url($user->image) }}">

    @if ($user->name != null)
      <div>Name: {{ $user->name }}</div>
    @endif

    <div>Username: {{ $user->username }}</div>
    <div>Email: {{ $user->email }}</div>
    {{-- <div>Password {{ $user->password }}</div> --}}
    <div id="user-actions-container">
      @if (auth()->id() == $user->id)
        <a class="link" href="{{ route('users.edit', $user->id) }}">
          <button class="button" id="edit-btn">Edit</button>
        </a>
        <form action="{{ route('users.destroy', $user->id) }}" method="post">
          @csrf
          @method('delete')
          <input type="submit" value="Delete Account" class="button" id="delete-btn">
        </form>
      @endif
    </div>
  </div>
@endsection
