@extends('layouts.page')
@Vite('resources/css/form.css')

@section('title', 'Edit')

@section('content')
  <div class="container">
    <form action="{{ route('users.update', $user->id) }}" method="POST"
      enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <label for="name">Name: </label>
      <input type="text" name="name" id="name" value="{{ $user->name }}">
      <label for="username">Username: </label>
      <input type="text" name="username" id="username" value="{{ $user->username }}">
      <label for="email">Email: </label>
      <input type="email" name="email" id="email" value="{{ $user->email }}">
      <label for="image">Image: </label>
      <input type="file" name="image" id="image">
      <input type="submit" value="Update">
    </form>

    @if ($errors->any())
      <div>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
@endsection
