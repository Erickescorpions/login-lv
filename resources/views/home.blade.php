@extends('layouts.page')
@Vite('resources/css/home.css')

@section('title', 'Home')

@section('content')
 
<h1>Hello world</h1>
<p>Welcome {{ auth()->user()->username }}.</p>

<h2>Registered users</h2>
<div id="user-cards-container">
  @foreach ($users as $user) 
    <a class="user-card" href="{{ route('users.show', $user->id) }}">
      <div class="user-card-title"><h2>{{ $user->username }}</h2></div>
      <div class="user-card-image"><img class="user-img" src="{{ Storage::url($user->image) }}"></div>
    </a>
  @endforeach
</div>
@endsection
